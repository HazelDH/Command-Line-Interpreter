#include "cli.h"
#include "usart.h"
#include <string.h>
#include <ctype.h>

// head of CLI command list
static CLI_CommandItem *head = NULL;

// char buffer where command will be stored
static char commandBuffer[100];

/**
 * This function searches the CLI command list and tries to find a descriptor for the provided command.
 * The command format is case-insensitive.
 * Returns pointer to @ref CLI_MenuItem structure if given command was found in the CLI command list.
 *
 * @param command pointer to command (string)
 *
 * @retval pointer to @ref CLI_MenuItem structure desrcibing the command, if command was found
 * @retval NULL if the given command does not match any command regitstered in the CLI command list 
 */
static CLI_CommandItem* CLI_GetMenuItemByCommandName(char *command);

/**
 * @bref This function is responsible for collecting the command reading in from USART.
 *
 * This function should check wether the USART interface has some new data. If it does
 * this function reads new characters and stores them in a command buffer. This function
 * is also responsible for providing echo of the input characters back to the buffer.
 *
 * The function exits when:
 * - no more characters are available to read from USART - the function returns false
 * - new line was detected - this function returns true
 *
 * @retval true if command was collected
 * @retval false if command is yet incomplete
 */
static bool CLI_StoreCommand(void);

/**
 * @brief This function converts string to a lowercase
 *
 * @param dst pointer where converted null terminated string will be stored
 * @param src pointer to string which will be converted
 */
static void CLI_StringToLower(char *dst, const char *src);
	
	
	
void CLI_Proc(void){
	if (CLI_StoreCommand()) {
		CLI_CommandItem* ptr = CLI_GetMenuItemByCommandName(commandBuffer);
		if (ptr == NULL) {
			USART_WriteString("Wrong command\r\n");
	  } else {
			ptr->callback(commandBuffer + strlen(ptr->commandName));
		}
	}
}

bool CLI_AddCommand(CLI_CommandItem *item){
	if ((!item) && (item->callback == NULL) && (item->commandName == NULL)) { 
		return false; 
	}
	item->next = NULL;
	if (head == NULL) {
		head = item;
	} else {
		CLI_CommandItem *ptr = head;
		while (ptr->next != NULL) {
			ptr = ptr->next;
		}
		ptr->next = item;
	}
	return true;
}

void CLI_PrintAllCommands(void){
	CLI_CommandItem *ptr = head;
	while (ptr != NULL) {
		USART_WriteString(ptr->commandName);
		USART_PutChar('\r');
		USART_PutChar('\n');
		ptr = ptr->next;
	}
}

CLI_CommandItem* CLI_GetMenuItemByCommandName(char *command){
	CLI_CommandItem *ptr = head;
	while(ptr != NULL) {
		CLI_StringToLower(command, command);
		char lowerCommandName[100];
		CLI_StringToLower(lowerCommandName, ptr->commandName);
		if (memcmp(command, lowerCommandName, strchr(command, ' ') - command) == 0) return ptr;
		ptr = ptr->next;
	}		
	return NULL;
};

void CLI_StringToLower(char *dst, const char *src){
	int i = 0;
	while( src[i] != '\0'){
		dst[i]=tolower(src[i]);
		++i;
	}
	dst[i] = '\0';
}

bool CLI_StoreCommand(){
	char c;
	static int i = 0;
	while(USART_GetChar(&c)){ 
		commandBuffer[i++] = c;
		USART_PutChar(c);
		if(c == '\r') {
			USART_PutChar('\n');
			commandBuffer[i-1] = '\0';
			i = 0;
			return true;
		}
	}
	return false;
}

